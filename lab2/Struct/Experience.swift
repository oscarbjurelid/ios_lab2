//
//  Experience.swift
//  lab2
//
//  Created by Oscar Bjurelid on 2018-12-02.
//  Copyright © 2018 Oscar Bjurelid. All rights reserved.
//

import Foundation

struct Experience {
    var imageName: String
    var work: String
    var year: String
    var description: String
}
