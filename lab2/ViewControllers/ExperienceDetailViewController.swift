//
//  ExperienceDetailViewController.swift
//  lab2
//
//  Created by Oscar Bjurelid on 2018-12-02.
//  Copyright © 2018 Oscar Bjurelid. All rights reserved.
//

import UIKit

class ExperienceDetailViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var infoLabelView: UILabel!
    @IBOutlet weak var experienceImageView: UIImageView!
    
    var experience: Experience = Experience(imageName: "", work: "", year: "", description: "")
    
    // MARK: - Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = experience.work
        experienceImageView.image = UIImage(named: experience.imageName)
        infoLabelView.text = experience.description

        
    }
    



}
