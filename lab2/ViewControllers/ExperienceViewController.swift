//
//  ExperienceViewController.swift
//  lab2
//
//  Created by Oscar Bjurelid on 2018-12-02.
//  Copyright © 2018 Oscar Bjurelid. All rights reserved.
//

import UIKit

class ExperienceViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    var workExperience: [Experience] = []
    var educationExperience: [Experience] = []
    var experiences: [[Experience]] = []
    var sectionName: [String] = []
    
    
    
    // MARK: - View controller specifics
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Experience"
        
        workExperience = [
            Experience(imageName: "hyresbo", work: "Hyresbostäder", year: "2016-2018", description: "master of Vectmeistroz"),
            Experience(imageName: "greentable", work: "GreenTable AS", year: "2015", description: "Bära möbler ger bra beachkropp."),
            Experience(imageName: "school", work: "Kyrkerörsskolan", year: "2016", description: "Personliga egot har aldrig varit större efter att ha präntant in de fysikaliska lagarna till 15 åriga kids.")
        ]
        educationExperience = [
            Experience(imageName: "alleberg", work: "Ållebergsgymnasiet", year: "2010-2013", description: "Fick möjligheten att praktisera snorungehet alldelles för länge. Men trevligt var det!"),
            Experience(imageName: "johndeere", work: "Tidaholms maskin utbildning", year: "2015", description: "Är man från schlätta så är en truckutbildning obligatoriskt. Första dagen välte jag en pall som var värd över miljonen. Kunde aldrig se chefen i ögonen efter den dagen."),
            Experience(imageName: "ju", work: "JU Computer Science", year: "2017-2020", description: "Fått ynneseten att ta del av den fabolösa kursen iOS-Development, utförd av En App Studio.")
        ]
        experiences = [workExperience, educationExperience]
        sectionName = ["Work Experience", "Education"]
        
        tableView.tableFooterView = UIView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ExperienceDetailViewController {
            if let indexPath = sender as? IndexPath {
                let experience = experiences[indexPath.section][indexPath.row]
                destination.experience = experience
            }
        }
    }
    

}

// MARK: - TableView delegates
extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experiences[section].count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceTableViewCell", for: indexPath) as? ExperienceTableViewCell {
            let experience = experiences[indexPath.section][indexPath.row]
            cell.workLabel.text = experience.work
            cell.yearLabel.text = experience.year
            cell.experienceImage.image = UIImage(named: experience.imageName)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        headerView.backgroundColor = UIColor(named: "blueByOscar")
        
        let headerLabel = UILabel(frame: CGRect(x: 8, y: 0, width: headerView.frame.width - 16, height: 30))
        headerLabel.text = sectionName[section]
        headerLabel.textColor = UIColor(named: "greenByOscar")
        headerLabel.font = UIFont.boldSystemFont(ofSize: 14)
        headerLabel.textAlignment = .center
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailSegue", sender: indexPath)
    }
}
