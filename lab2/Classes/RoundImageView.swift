//
//  RoundImageView.swift
//  lab2
//
//  Created by Oscar Bjurelid on 2018-11-06.
//  Copyright © 2018 Oscar Bjurelid. All rights reserved.
//

import UIKit

@IBDesignable
class RoundImageView: UIImageView {

    @IBInspectable
    public var borderColor: UIColor = UIColor.red {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable
    public var borderWidth: CGFloat = 1.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder :aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func layoutSubviews() {
        self.layer.cornerRadius = self.frame.size.height / 4
        self.clipsToBounds = true
    }

}
