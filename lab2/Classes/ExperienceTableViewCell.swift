//
//  ExperienceTableViewCell.swift
//  lab2
//
//  Created by Oscar Bjurelid on 2018-12-02.
//  Copyright © 2018 Oscar Bjurelid. All rights reserved.
//

import UIKit

class ExperienceTableViewCell: UITableViewCell {

    
    // MARK: - Outlets
    @IBOutlet weak var experienceImage: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var workLabel: UILabel!
    
    // MARK: - Functions
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
